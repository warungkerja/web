<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

	<!-- Stylesheets
	============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700,800,900|Montserrat:400,700|Abril+Fatface|Yellowtail&display=swap" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/css/bootstrap.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/style.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/css/dark.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/css/font-icons.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/css/animate.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/css/magnific-popup.css')}}" type="text/css" />

	<link rel="stylesheet" href="{{asset('semicolon/css/custom.css')}}" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
	<link rel="stylesheet" type="text/css" href="{{asset('semicolon/include/rs-plugin/css/settings.css')}}" media="screen" />
	<link rel="stylesheet" type="text/css" href="{{asset('semicolon/include/rs-plugin/css/layers.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('semicolon/include/rs-plugin/css/navigation.css')}}">

	<!-- ADD-ONS CSS FILES -->
	<link rel="stylesheet" type="text/css" href="{{asset('semicolon/include/rs-plugin/css/addons/revolution.addon.revealer.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('semicolon/include/rs-plugin/css/addons/revolution.addon.revealer.preloaders.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('semicolon/include/rs-plugin/css/addons/revolution.addon.particles.css')}}">

	<!-- Document Title
	============================================= -->
	<title>{{ config('app.name') }}</title>

	<style>

		.demos-filter {
			margin: 0;
			text-align: right;
		}

		.demos-filter li {
			list-style: none;
			margin: 10px 0px;
		}

		.demos-filter li a {
			display: block;
			border: 0;
			text-transform: uppercase;
			letter-spacing: 1px;
			color: #444;
		}

		.demos-filter li a:hover,
		.demos-filter li.activeFilter a { color: #1ABC9C; }

		@media (max-width: 991px) {
			.demos-filter { text-align: center; }

			.demos-filter li {
				float: left;
				width: 33.3%;
				padding: 0 20px;
			}
		}

		@media (max-width: 767px) { .demos-filter li { width: 50%; } }

		#welcome_wrapper .tp-loader.spinner2{ background-color: #006dd2 !important; }video::-webkit-media-controls-start-playback-button{display:none !important}.tc-gradient i:before{background:-webkit-linear-gradient(left,#5e05b1 0%,#09ceff 100%);-webkit-background-clip:text;-webkit-text-fill-color:transparent}.tc-btnshadow{-webkit-box-shadow:3px 5px 10px 0px rgba(0,0,0,0.2) !important;-moz-box-shadow:3px 5px 10px 0px rgba(0,0,0,0.2) !important;box-shadow:3px 5px 10px 0px rgba(0,0,0,0.2) !important}#welcome .uranus.tparrows{width:50px; height:50px; background:rgba(255,255,255,0)}#welcome .uranus.tparrows:before{width:50px; height:50px; line-height:50px; font-size:40px; transition:all 0.3s;-webkit-transition:all 0.3s}#welcome .uranus.tparrows:hover:before{opacity:0.75}

	</style>

</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Slider
		============================================= -->
		<section id="slider" class="slider-element revslider-wrap h-auto include-header">

			<div id="welcome_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="reveal-add-on39" data-source="gallery" style="background:#ffffff;padding:0px;">
				<!-- START REVOLUTION SLIDER 5.4.6 fullscreen mode -->
				<div id="welcome" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.6">
					<ul>	<!-- SLIDE  -->
						<li class="dark" data-index="rs-243" data-transition="fadethroughdark" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="{{asset('semicolon/include/rs-plugin/demos/assets/images/purpleway.jpg')}}"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
							<!-- MAIN IMAGE -->
							<img src="{{asset('semicolon/include/rs-plugin/demos/assets/images/dummy.png')}}"  alt="Image"  data-lazyload="{{asset('semicolon/include/rs-plugin/demos/assets/images/purpleway.jpg')}}" data-bgposition="center center" data-kenburns="on" data-duration="4000" data-ease="Power3.easeInOut" data-scalestart="150" data-scaleend="100" data-rotatestart="0" data-rotateend="0" data-blurstart="0" data-blurend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="4" class="rev-slidebg" data-no-retina>
							<!-- LAYERS -->

							<!-- LAYER NR. 1 -->
							<div class="tp-caption tp-shape tp-shapewrapper "
								 id="slide-243-layer-14"
								 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
								 data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
											data-width="full"
								data-height="full"
								data-whitespace="nowrap"

								data-type="shape"
								data-basealign="slide"
								data-responsive_offset="off"
								data-responsive="off"
								data-frames='[{"delay":10,"speed":1000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1500,"frame":"999","to":"opacity:0;","ease":"Power4.easeIn"}]'
								data-textAlign="['inherit','inherit','inherit','inherit']"
								data-paddingtop="[0,0,0,0]"
								data-paddingright="[0,0,0,0]"
								data-paddingbottom="[0,0,0,0]"
								data-paddingleft="[0,0,0,0]"

								style="z-index: 5;font-family:Open Sans;background:linear-gradient(180deg, rgba(0,0,0,0.85) 0%, rgba(0,0,0,0) 100%);"> </div>

							<!-- LAYER NR. 2 -->
							<div class="tp-caption  "
								 id="slide-243-layer-1"
								 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
								 data-y="['middle','middle','middle','middle']" data-voffset="['-90','-90','-120','-140']"
											data-fontsize="['80','80','70','60']"
								data-lineheight="['80','80','70','60']"
								data-letterspacing="['-5','-5','-2','-3']"
								data-width="['800','none','360','260']"
								data-height="none"
								data-whitespace="['normal','nowrap','normal','normal']"

								data-type="text"
								data-responsive_offset="off"
								data-responsive="off"
								data-frames='[{"delay":900,"speed":2000,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:1.1;sY:1.1;skX:0;skY:0;opacity:0;","color":"#000000","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","color":"#000000","to":"opacity:0;","ease":"nothing"}]'
								data-textAlign="['center','left','center','center']"
								data-paddingtop="[0,0,0,0]"
								data-paddingright="[10,10,0,0]"
								data-paddingbottom="[0,0,0,0]"
								data-paddingleft="[0,0,0,0]"

								style="z-index: 6; min-width: 800px; max-width: 800px; white-space: normal; font-size: 80px; line-height: 80px; font-weight: 600; color: #ffffff; letter-spacing: -5px;font-family:Montserrat;">Warung Kerja</div>

							<!-- LAYER NR. 3 -->
							<div class="tp-caption  "
								 id="slide-243-layer-2"
								 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
								 data-y="['middle','middle','middle','middle']" data-voffset="['20','20','20','0']"
											data-fontsize="['20','20','20','17']"
								data-lineheight="['30','30','25','22']"
								data-color="['rgba(255,255,255,0.75)','rgba(255,255,255,0.65)','rgba(255,255,255,0.65)','rgba(255,255,255,0.65)']"
								data-width="['640','481','360','260']"
								data-height="none"
								data-whitespace="normal"

								data-type="text"
								data-responsive_offset="off"
								data-responsive="off"
								data-frames='[{"delay":900,"speed":2000,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:1.1;sY:1.1;skX:0;skY:0;opacity:0;","color":"#000000","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","color":"#000000","to":"opacity:0;","ease":"nothing"}]'
								data-textAlign="['center','center','center','center']"
								data-paddingtop="[0,0,0,0]"
								data-paddingright="[0,0,0,0]"
								data-paddingbottom="[0,0,0,0]"
								data-paddingleft="[0,0,0,0]"

								style="z-index: 7; min-width: 640px; max-width: 640px; white-space: normal; font-size: 20px; line-height: 30px; font-weight: 400; color: rgba(255,255,255,0.75);font-family:Montserrat;">
                                We've Got You Covered. From Designers, Developers and Writers to Admins and Accountants. The Choice Is Yours. Post a Project Now to Get Started.
                            </div>

							<!-- LAYER NR. 4 -->
							{{-- <a class="tp-caption rev-btn btn btn-block tc-btnshadow tp-rs-menulink"
					 			href="http://themes.semicolonweb.com/html/canvas/intro.php#section-niche" target="_blank"
					 			id="slide-243-layer-12"
								 data-x="['center','center','center','center']" data-hoffset="['-150','-150','0','0']"
								 data-y="['middle','middle','middle','middle']" data-voffset="['150','140','130','100']"
											data-lineheight="['60','60','50','50']"
								data-width="['280','280','200','none']"
								data-height="none"
								data-whitespace="nowrap"

								data-type="button"
								data-actions=''
								data-responsive_offset="off"
								data-responsive="off"
								data-frames='[{"delay":900,"speed":2000,"frame":"0","from":"x:-50px;z:0;rX:0;rY:0;rZ:0;sX:1.1;sY:1.1;skX:0;skY:0;opacity:0;fbr:100;","bgcolor":"#000000","to":"o:1;fbr:100;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","bgcolor":"#000000","to":"opacity:0;fbr:100;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"150","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;fbr:90%;","style":"c:rgba(255,255,255,1);"}]'
								data-textAlign="['center','center','center','inherit']"
								data-paddingtop="[0,0,0,0]"
								data-paddingright="[50,50,20,20]"
								data-paddingbottom="[0,0,0,0]"
								data-paddingleft="[50,50,20,20]"

								style="z-index: 8; min-width: 280px; max-width: 280px; white-space: nowrap; font-size: 17px; line-height: 60px; font-weight: 400; color: rgba(255,255,255,1);font-family:Montserrat;background-color:rgb(0,109,210);border-radius:30px 30px 30px 30px;outline:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;"><i class="icon-cube" style="font-size:17px;margin-right:5px;"></i> See Demos </a> --}}

							<!-- LAYER NR. 5 -->
							{{-- <a class="tp-caption rev-btn   tc-btnshadow"
								href="https://themeforest.net/item/canvas-the-multipurpose-html5-template/9228123?ref=SemiColonWeb&license=regular&open_purchase_for_item_id=9228123&purchasable=source" target="_blank"
								id="slide-243-layer-13"
								 data-x="['center','center','center','center']" data-hoffset="['150','150','0','0']"
								 data-y="['middle','middle','middle','middle']" data-voffset="['150','140','190','160']"
											data-lineheight="['60','60','50','50']"
								data-width="['280','280','200','none']"
								data-height="none"
								data-whitespace="nowrap"

								data-type="button"
								data-actions=''
								data-responsive_offset="off"
								data-responsive="off"
								data-frames='[{"delay":900,"speed":2000,"frame":"0","from":"x:50px;z:0;rX:0;rY:0;rZ:0;sX:1.1;sY:1.1;skX:0;skY:0;opacity:0;fbr:100;","bgcolor":"#000000","to":"o:1;fbr:100;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","bgcolor":"#000000","to":"opacity:0;fbr:100;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"150","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;fbr:90%;","style":"c:rgba(255,255,255,1);"}]'
								data-textAlign="['center','center','center','inherit']"
								data-paddingtop="[0,0,0,0]"
								data-paddingright="[50,50,20,20]"
								data-paddingbottom="[0,0,0,0]"
								data-paddingleft="[50,50,20,20]"

								style="z-index: 9; min-width: 280px; max-width: 280px; white-space: nowrap; font-size: 17px; line-height: 60px; font-weight: 400; color: rgba(255,255,255,1);font-family:Montserrat;background-color:rgb(91,17,184);border-radius:30px 30px 30px 30px;outline:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;"><i class="icon-cloud-download" style="font-size:17px;margin-right:5px;"></i> Get a License </a> --}}
						</li>
						<!-- SLIDE  -->
						<li class="dark" data-index="rs-245" data-transition="fadethroughdark" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="{{asset('semicolon/include/rs-plugin/demos/assets/images/citybg.jpg')}}"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
							<!-- MAIN IMAGE -->
							<img src="{{asset('semicolon/include/rs-plugin/demos/assets/images/dummy.png')}}"  alt="Image"  data-lazyload="{{asset('semicolon/include/rs-plugin/demos/assets/images/citybg.jpg')}}" data-bgposition="center center" data-kenburns="on" data-duration="4000" data-ease="Power3.easeInOut" data-scalestart="150" data-scaleend="100" data-rotatestart="0" data-rotateend="0" data-blurstart="0" data-blurend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="4" class="rev-slidebg" data-no-retina>
							<!-- LAYERS -->

							<!-- LAYER NR. 11 -->
							<div class="tp-caption tp-shape tp-shapewrapper "
								 id="slide-245-layer-14"
								 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
								 data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
											data-width="full"
								data-height="full"
								data-whitespace="nowrap"

								data-type="shape"
								data-basealign="slide"
								data-responsive_offset="off"
								data-responsive="off"
								data-frames='[{"delay":10,"speed":1000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1500,"frame":"999","to":"opacity:0;","ease":"Power4.easeIn"}]'
								data-textAlign="['inherit','inherit','inherit','inherit']"
								data-paddingtop="[0,0,0,0]"
								data-paddingright="[0,0,0,0]"
								data-paddingbottom="[0,0,0,0]"
								data-paddingleft="[0,0,0,0]"

								style="z-index: 5;font-family:Open Sans;background:linear-gradient(180deg, rgba(0,0,0,0.85) 0%, rgba(0,0,0,0) 100%);"> </div>

							<!-- LAYER NR. 12 -->
							<div class="tp-caption  "
								 id="slide-245-layer-1"
								 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
								 data-y="['middle','middle','middle','middle']" data-voffset="['-90','-90','-120','-140']"
											data-fontsize="['90','80','70','60']"
								data-lineheight="['90','80','70','60']"
								data-letterspacing="['-5','-5','-2','-3']"
								data-width="['800','none','360','260']"
								data-height="none"
								data-whitespace="['normal','nowrap','normal','normal']"

								data-type="text"
								data-responsive_offset="off"
								data-responsive="off"
								data-frames='[{"delay":900,"speed":2000,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:1.1;sY:1.1;skX:0;skY:0;opacity:0;","color":"#000000","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","color":"#000000","to":"opacity:0;","ease":"nothing"}]'
								data-textAlign="['center','left','center','center']"
								data-paddingtop="[0,0,0,0]"
								data-paddingright="[10,10,0,0]"
								data-paddingbottom="[0,0,0,0]"
								data-paddingleft="[0,0,0,0]"

								style="z-index: 6; min-width: 800px; max-width: 800px; white-space: normal; font-size: 90px; line-height: 90px; font-weight: 600; color: #ffffff; letter-spacing: -5px;font-family:Montserrat;">Warung Kerja  </div>

							<!-- LAYER NR. 13 -->
							<div class="tp-caption mt-5"
								 id="slide-245-layer-2"
								 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
								 data-y="['middle','middle','middle','middle']" data-voffset="['20','20','20','0']"
											data-fontsize="['20','20','20','17']"
								data-lineheight="['30','30','25','22']"
								data-color="['rgba(255,255,255,0.75)','rgba(255,255,255,0.65)','rgba(255,255,255,0.65)','rgba(255,255,255,0.65)']"
								data-width="['640','481','360','260']"
								data-height="none"
								data-whitespace="normal"

								data-type="text"
								data-responsive_offset="off"
								data-responsive="off"
								data-frames='[{"delay":900,"speed":2000,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:1.1;sY:1.1;skX:0;skY:0;opacity:0;","color":"#000000","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","color":"#000000","to":"opacity:0;","ease":"nothing"}]'
								data-textAlign="['center','center','center','center']"
								data-paddingtop="[0,0,0,0]"
								data-paddingright="[0,0,0,0]"
								data-paddingbottom="[0,0,0,0]"
								data-paddingleft="[0,0,0,0]"

								style="z-index: 7; min-width: 640px; max-width: 640px; white-space: normal; font-size: 20px; line-height: 30px; font-weight: 400; color: rgba(255,255,255,0.75);font-family:Montserrat;">
                                Download Millions of Video Templates, WP Themes, Photos, Graphic Templates & More! Deliver better Projects Faster with Ready-to-use Graphic, Web and Video Templates. Unlimited Downloads.
                            </div>

							<!-- LAYER NR. 14 -->
							{{-- <a class="tp-caption rev-btn   tc-btnshadow tp-rs-menulink"
								href="http://themes.semicolonweb.com/html/canvas/intro.php#section-niche" target="_blank"
								id="slide-245-layer-12"
								 data-x="['center','center','center','center']" data-hoffset="['-150','-150','0','0']"
								 data-y="['middle','middle','middle','middle']" data-voffset="['150','140','130','100']"
											data-lineheight="['60','60','50','50']"
								data-width="['280','280','200','none']"
								data-height="none"
								data-whitespace="nowrap"

								data-type="button"
								data-actions=''
								data-responsive_offset="off"
								data-responsive="off"
								data-frames='[{"delay":900,"speed":2000,"frame":"0","from":"x:-50px;z:0;rX:0;rY:0;rZ:0;sX:1.1;sY:1.1;skX:0;skY:0;opacity:0;fbr:100;","bgcolor":"#000000","to":"o:1;fbr:100;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","bgcolor":"#000000","to":"opacity:0;fbr:100;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"150","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;fbr:90%;","style":"c:rgba(255,255,255,1);"}]'
								data-textAlign="['center','center','center','inherit']"
								data-paddingtop="[0,0,0,0]"
								data-paddingright="[50,50,20,20]"
								data-paddingbottom="[0,0,0,0]"
								data-paddingleft="[50,50,20,20]"

								style="z-index: 8; min-width: 280px; max-width: 280px; white-space: nowrap; font-size: 17px; line-height: 60px; font-weight: 400; color: rgba(255,255,255,1);font-family:Montserrat;background-color:rgb(0,109,210);border-radius:30px 30px 30px 30px;outline:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;"><i class="icon-cube" style="font-size:17px;margin-right:5px;"></i> See Demos </a> --}}

							<!-- LAYER NR. 15 -->
							{{-- <a class="tp-caption rev-btn   tc-btnshadow"
								href="https://themeforest.net/item/canvas-the-multipurpose-html5-template/9228123?ref=SemiColonWeb&license=regular&open_purchase_for_item_id=9228123&purchasable=source" target="_blank"
								id="slide-245-layer-13"
								 data-x="['center','center','center','center']" data-hoffset="['150','150','0','0']"
								 data-y="['middle','middle','middle','middle']" data-voffset="['150','140','190','160']"
											data-lineheight="['60','60','50','50']"
								data-width="['280','280','200','none']"
								data-height="none"
								data-whitespace="nowrap"

								data-type="button"
								data-actions=''
								data-responsive_offset="off"
								data-responsive="off"
								data-frames='[{"delay":900,"speed":2000,"frame":"0","from":"x:50px;z:0;rX:0;rY:0;rZ:0;sX:1.1;sY:1.1;skX:0;skY:0;opacity:0;fbr:100;","bgcolor":"#000000","to":"o:1;fbr:100;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","bgcolor":"#000000","to":"opacity:0;fbr:100;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"150","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;fbr:90%;","style":"c:rgba(255,255,255,1);"}]'
								data-textAlign="['center','center','center','inherit']"
								data-paddingtop="[0,0,0,0]"
								data-paddingright="[50,50,20,20]"
								data-paddingbottom="[0,0,0,0]"
								data-paddingleft="[50,50,20,20]"

								style="z-index: 9; min-width: 280px; max-width: 280px; white-space: nowrap; font-size: 17px; line-height: 60px; font-weight: 400; color: rgba(255,255,255,1);font-family:Montserrat;background-color:rgb(91,17,184);border-radius:30px 30px 30px 30px;outline:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;"><i class="icon-cloud-download" style="font-size:17px;margin-right:5px;"></i> Get a License </a> --}}
						</li>
					</ul>
					<div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
				</div>
			</div><!-- END REVOLUTION SLIDER -->

		</section>

		<!-- Content
		============================================= -->
		<section id="content" style="display:none;">
			<div class="content-wrap">
				<div class="container clearfix">

					<a href="rs-demos.html" class="btn btn-secondary btn-lg btn-block mx-auto" style="max-width: 20rem;"><i class="icon-line-arrow-left mr-2" style="position: relative; top: 1px;"></i> Back to All Demos</a>

				</div>
			</div>
		</section><!-- #content end -->
	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- JavaScripts
	============================================= -->
	<script src="{{asset('semicolon/js/jquery.js')}}"></script>
	<script src="{{asset('semicolon/js/plugins.min.js')}}"></script>

	<!-- Footer Scripts
	============================================= -->
	<script src="{{asset('semicolon/js/functions.js')}}"></script>

	<!-- SLIDER REVOLUTION 5.x SCRIPTS  -->
	<script src="{{asset('semicolon/include/rs-plugin/js/jquery.themepunch.tools.min.js')}}"></script>
	<script src="{{asset('semicolon/include/rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>
	<!-- SLIDER REVOLUTION ADDONS  -->
	<script src="{{asset('semicolon/include/rs-plugin/js/addons/revolution.addon.revealer.min.js')}}"></script>
	<script src="{{asset('semicolon/include/rs-plugin/js/addons/revolution.addon.particles.min.js')}}"></script>
	<!-- SLIDER REVOLUTION EXTENSIONS  -->
	<script src="{{asset('semicolon/include/rs-plugin/js/extensions/revolution.extension.actions.min.js')}}"></script>
	<script src="{{asset('semicolon/include/rs-plugin/js/extensions/revolution.extension.carousel.min.js')}}"></script>
	<script src="{{asset('semicolon/include/rs-plugin/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
	<script src="{{asset('semicolon/include/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
	<script src="{{asset('semicolon/include/rs-plugin/js/extensions/revolution.extension.migration.min.js')}}"></script>
	<script src="{{asset('semicolon/include/rs-plugin/js/extensions/revolution.extension.navigation.min.js')}}"></script>
	<script src="{{asset('semicolon/include/rs-plugin/js/extensions/revolution.extension.parallax.min.js')}}"></script>
	<script src="{{asset('semicolon/include/rs-plugin/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
	<script src="{{asset('semicolon/include/rs-plugin/js/extensions/revolution.extension.video.min.js')}}"></script>

	<!-- ADD-ONS JS FILES -->
	<script>
		var revapi109,
		tpj=jQuery;
		var $ = jQuery.noConflict();

		tpj(document).ready(function() {
			if(tpj("#welcome").revolution == undefined){
				revslider_showDoubleJqueryError("#welcome");
			}else{
				revapi109 = tpj("#welcome").show().revolution({
					sliderType:"standard",
					jsFileLocation:"/include/rs-plugin/js/",
					sliderLayout:"fullscreen",
					dottedOverlay:"none",
					delay:9000,
					particles: {startSlide: "first", endSlide: "last", zIndex: "1",
						particles: {
							number: {value: 25}, color: {value: "#ffffff"},
							shape: {
								type: "circle", stroke: {width: 0, color: "#ffffff", opacity: 1},
								image: {src: ""}
							},
							opacity: {value: 1, random: true, min: 0.25, anim: {enable: true, speed: 3, opacity_min: 0, sync: false}},
							size: {value: 2, random: true, min: 0.5, anim: {enable: true, speed: 5, size_min: 1, sync: false}},
							line_linked: {enable: false, distance: 150, color: "#ffffff", opacity: 0.4, width: 1},
							move: {enable: true, speed: 2, direction: "none", random: true, min_speed: 1, straight: false, out_mode: "out"}},
						interactivity: {
							events: {onhover: {enable: false, mode: "bubble"}, onclick: {enable: false, mode: "repulse"}},
							modes: {grab: {distance: 400, line_linked: {opacity: 0.5}}, bubble: {distance: 400, size: 40, opacity: 0.5}, repulse: {distance: 200}}
						}
					},
					revealer: {
						direction: "open_horizontal",
						color: "#ffffff",
						duration: "1500",
						delay: "0",
						easing: "Power2.easeInOut",
						overlay_enabled: true,
						overlay_color: "#000000",
						overlay_duration: "1500",
						overlay_delay: "0",
						overlay_easing: "Power2.easeInOut",
						spinner: "1",
						spinnerColor: "#006dd2",
					},
					navigation: {
						keyboardNavigation:"off",
						keyboard_direction: "horizontal",
						mouseScrollNavigation:"off",
						 mouseScrollReverse:"default",
						onHoverStop:"off",
						arrows: {
							style:"uranus",
							enable:true,
							hide_onmobile:false,
							hide_onleave:false,
							tmp:'',
							left: {
								h_align:"left",
								v_align:"center",
								h_offset:10,
								v_offset:0
							},
							right: {
								h_align:"right",
								v_align:"center",
								h_offset:10,
								v_offset:0
							}
						}
					},
					viewPort: {
						enable:true,
						outof:"wait",
						visible_area:"80%",
						presize:true
					},
					responsiveLevels:[1240,1024,778,480],
					visibilityLevels:[1240,1024,778,480],
					gridwidth:[1240,1024,778,480],
					gridheight:[868,768,960,720],
					lazyType:"single",
					parallax: {
						type:"scroll",
						origo:"slidercenter",
						speed:400,
					  speedbg:0,
					  speedls:0,
						levels:[5,10,15,20,25,30,35,40,45,46,47,48,49,50,51,55],
					},
					shadow:0,
					spinner:"spinner2",
					stopLoop:"on",
					stopAfterLoops:0,
					stopAtSlide:1,
					shuffle:"off",
					autoHeight:"off",
					fullScreenAutoWidth:"off",
					fullScreenAlignForce:"off",
					fullScreenOffsetContainer: "",
					fullScreenOffset: "",
					disableProgressBar:"on",
					hideThumbsOnMobile:"off",
					hideSliderAtLimit:0,
					hideCaptionAtLimit:0,
					hideAllCaptionAtLilmit:0,
					debugMode:false,
					fallbacks: {
						simplifyAll:"off",
						nextSlideOnWindowFocus:"off",
						disableFocusListener:false,
					}
				});

				window.RsAddonRevealerCustom = {
					itm_2: {
						direction: "open_vertical",
						color: "#006dd2",
						duration: "1500",
						delay: "0",
						easing: "Power2.easeInOut",
						overlay_enabled: true,
						overlay_color: "#000000",
						overlay_duration: "1500",
						overlay_delay: "0",
						overlay_easing: "Power3.easeInOut",
						spinner: "2",
						spinnerColor: "rgba(255,255,255,",
						spinnerHTML: "<div class='rsaddon-revealer-spinner rsaddon-revealer-spinner-2'><div class='rsaddon-revealer-2' style='border-top-color: {color}0.65); border-bottom-color: {color}0.15); border-left-color: {color}0.65); border-right-color: {color}0.15)'><\/div><\/div>"
					},
					itm_3: {
						direction: "split_left_corner",
						color: "#5b11b8",
						duration: "1500",
						delay: "0",
						easing: "Power2.easeInOut",
						overlay_enabled: true,
						overlay_color: "#000000",
						overlay_duration: "1500",
						overlay_delay: "0",
						overlay_easing: "Power2.easeInOut",
						spinner: "3",
						spinnerColor: "#ffffff",
						spinnerHTML: "<div class='rsaddon-revealer-spinner rsaddon-revealer-spinner-3'><div class='rsaddon-revealer-3'><span style='background:{color}'><\/span><span style='background:{color}'><\/span><span style='background:{color}'><\/span><span style='background:{color}'><\/span><\/div><\/div>"
					},
					itm_4: {
						direction: "split_right_corner",
						color: "#ffffff",
						duration: "1500",
						delay: "0",
						easing: "Power2.easeInOut",
						overlay_enabled: true,
						overlay_color: "#006dd2",
						overlay_duration: "1500",
						overlay_delay: "0",
						overlay_easing: "Power2.easeInOut",
						spinner: "4",
						spinnerColor: "#006dd2",
						spinnerHTML: "<div class='rsaddon-revealer-spinner rsaddon-revealer-spinner-4'><div class='rsaddon-revealer-4'><span style='background:{color}'><\/span><span style='background:{color}'><\/span><\/div><\/div>"
					},
					itm_5: {
						direction: "shrink_circle",
						color: "#ffffff",
						duration: "1500",
						delay: "0",
						easing: "Power2.easeOut",
						overlay_enabled: true,
						overlay_color: "#000000",
						overlay_duration: "1500",
						overlay_delay: "0",
						overlay_easing: "Power2.easeInOut",
						spinner: "5",
						spinnerColor: "#000000",
						spinnerHTML: "<div class='rsaddon-revealer-spinner rsaddon-revealer-spinner-5'><div class='rsaddon-revealer-5'><span style='background:{color}'><\/span> <span style='background:{color}'><\/span> <span style='background:{color}'><\/span> <span style='background:{color}'><\/span> <span style='background:{color}'><\/span><\/div><\/div>"
					},
					itm_6: {
						direction: "expand_circle",
						color: "#000000",
						duration: "1500",
						delay: "0",
						easing: "Power2.easeInOut",
						overlay_enabled: true,
						overlay_color: "#5b11b8",
						overlay_duration: "1500",
						overlay_delay: "0",
						overlay_easing: "Power2.easeInOut",
						spinner: "6",
						spinnerColor: "#ffffff",
						spinnerHTML: "<div class='rsaddon-revealer-spinner rsaddon-revealer-spinner-6'><div class='rsaddon-revealer-6'><span style='background:{color}'><\/span><span style='background:{color}'><\/span><span style='background:{color}'><\/span><span style='background:{color}'><\/span><span style='background:{color}'><\/span><span style='background:{color}'><\/span><span style='background:{color}'><\/span><span style='background:{color}'><\/span><span style='background:{color}'><\/span><\/div><\/div>"
					},
					itm_7: {
						direction: "left_to_right",
						color: "#ffffff",
						duration: "1500",
						delay: "0",
						easing: "Power2.easeInOut",
						overlay_enabled: true,
						overlay_color: "#000000",
						overlay_duration: "2500",
						overlay_delay: "0",
						overlay_easing: "Power2.easeInOut",
						spinner: "7",
						spinnerColor: "#006dd2",
						spinnerHTML: "<div class='rsaddon-revealer-spinner rsaddon-revealer-spinner-7'><div class='rsaddon-revealer-7' style='background:{color}'><\/div><\/div>"
					},
					itm_8: {
						direction: "right_to_left",
						color: "#ffffff",
						duration: "1500",
						delay: "0",
						easing: "Power2.easeInOut",
						overlay_enabled: true,
						overlay_color: "#006dd2",
						overlay_duration: "2500",
						overlay_delay: "0",
						overlay_easing: "Power2.easeInOut",
						spinner: "8",
						spinnerColor: "#006dd2",
						spinnerHTML: "<div class='rsaddon-revealer-spinner rsaddon-revealer-spinner-8'><div class='rsaddon-revealer-8'><span style='background:{color}'><\/span><span style='background:{color}'><\/span><span style='background:{color}'><\/span><span style='background:{color}'><\/span><\/div><\/div>"
					},
					itm_9: {
						direction: "top_to_bottom",
						color: "#ffffff",
						duration: "1500",
						delay: "0",
						easing: "Power2.easeInOut",
						overlay_enabled: true,
						overlay_color: "#5b11b8",
						overlay_duration: "2500",
						overlay_delay: "0",
						overlay_easing: "Power2.easeInOut",
						spinner: "9",
						spinnerColor: "#006dd2",
						spinnerHTML: "<div class='rsaddon-revealer-spinner rsaddon-revealer-spinner-9'><div class='rsaddon-revealer-9'><span style='background:{color}'><\/span><span style='background:{color}'><\/span><span style='background:{color}'><\/span><span style='background:{color}'><\/span><\/div><\/div>"
					},
					itm_10: {
						direction: "bottom_to_top",
						color: "#000000",
						duration: "1500",
						delay: "0",
						easing: "Power2.easeInOut",
						overlay_enabled: true,
						overlay_color: "#ffffff",
						overlay_duration: "2500",
						overlay_delay: "0",
						overlay_easing: "Power2.easeInOut",
						spinner: "10",
						spinnerColor: "#ffffff",
						spinnerHTML: "<div class='rsaddon-revealer-spinner rsaddon-revealer-spinner-10'><div class='rsaddon-revealer-10'><span style='background:{color}'><\/span><span style='background:{color}'><\/span><\/div><\/div>"
					},
					itm_11: {
						direction: "tlbr_skew",
						color: "#000000",
						duration: "1500",
						delay: "0",
						easing: "Power2.easeInOut",
						overlay_enabled: true,
						overlay_color: "#006dd2",
						overlay_duration: "1500",
						overlay_delay: "0",
						overlay_easing: "Power2.easeInOut",
						spinner: "1",
						spinnerColor: "#006dd2",
						spinnerHTML: "<div class='rsaddon-revealer-spinner rsaddon-revealer-spinner-1'><div class='rsaddon-revealer-1'><span style='background: {color}'><\/span><span style='background: {color}'><\/span><span style='background: {color}'><\/span><span style='background: {color}'><\/span><span style='background: {color}'><\/span><span style='background: {color}'><\/span><span style='background: {color}'><\/span><span style='background: {color}'><\/span><span style='background: {color}'><\/span><span style='background: {color}'><\/span><\/div><\/div \/>"
					},
					itm_12: {
						direction: "trbl_skew",
						color: "#000000",
						duration: "1500",
						delay: "0",
						easing: "Power2.easeInOut",
						overlay_enabled: true,
						overlay_color: "#5b11b8",
						overlay_duration: "1500",
						overlay_delay: "0",
						overlay_easing: "Power2.easeInOut",
						spinner: "2",
						spinnerColor: "rgba(255,255,255,",
						spinnerHTML: "<div class='rsaddon-revealer-spinner rsaddon-revealer-spinner-2'><div class='rsaddon-revealer-2' style='border-top-color: {color}0.65); border-bottom-color: {color}0.15); border-left-color: {color}0.65); border-right-color: {color}0.15)'><\/div><\/div>"
					},
					itm_13: {
						direction: "bltr_skew",
						color: "#ffffff",
						duration: "1500",
						delay: "0",
						easing: "Power2.easeInOut",
						overlay_enabled: true,
						overlay_color: "#000000",
						overlay_duration: "1500",
						overlay_delay: "0",
						overlay_easing: "Power2.easeInOut",
						spinner: "3",
						spinnerColor: "#006dd2",
						spinnerHTML: "<div class='rsaddon-revealer-spinner rsaddon-revealer-spinner-3'><div class='rsaddon-revealer-3'><span style='background:{color}'><\/span><span style='background:{color}'><\/span><span style='background:{color}'><\/span><span style='background:{color}'><\/span><\/div><\/div>"
					},
					itm_14: {
						direction: "brtl_skew",
						color: "#000000",
						duration: "1500",
						delay: "0",
						easing: "Power2.easeInOut",
						overlay_enabled: true,
						overlay_color: "#5b11b8",
						overlay_duration: "1500",
						overlay_delay: "0",
						overlay_easing: "Power2.easeInOut",
						spinner: "4",
						spinnerColor: "#5b11b8",
						spinnerHTML: "<div class='rsaddon-revealer-spinner rsaddon-revealer-spinner-4'><div class='rsaddon-revealer-4'><span style='background:{color}'><\/span><span style='background:{color}'><\/span><\/div><\/div>"
					}
				};

				jQuery('body').on('click','.itm', function() {
					window.location.hash = '?' + this.id;
					return false;
				});
				window.addEventListener('hashchange', function() {
					window.location.reload();
				});

			}

			RsParticlesAddOn(revapi109);

			RsRevealerAddOn(tpj, revapi109, "<div class='rsaddon-revealer-spinner rsaddon-revealer-spinner-1'><div class='rsaddon-revealer-1'><span style='background: {color}'><\/span><span style='background: {color}'><\/span><span style='background: {color}'><\/span><span style='background: {color}'><\/span><span style='background: {color}'><\/span><span style='background: {color}'><\/span><span style='background: {color}'><\/span><span style='background: {color}'><\/span><span style='background: {color}'><\/span><span style='background: {color}'><\/span><\/div><\/div \/>");
		});	/*ready*/
	</script>
        <!-- The core Firebase JS SDK is always required and must be listed first -->
    <script src="https://www.gstatic.com/firebasejs/8.3.3/firebase-app.js"></script>

    <!-- TODO: Add SDKs for Firebase products that you want to use
        https://firebase.google.com/docs/web/setup#available-libraries -->
    <script src="https://www.gstatic.com/firebasejs/8.3.3/firebase-analytics.js"></script>

    <script>
    // Your web app's Firebase configuration
    // For Firebase JS SDK v7.20.0 and later, measurementId is optional
    var firebaseConfig = {
        apiKey: "AIzaSyCQ6RVrInkYq1bi-F6z4W6iuj3BhUg_Hnw",
        authDomain: "warung-kerja.firebaseapp.com",
        projectId: "warung-kerja",
        storageBucket: "warung-kerja.appspot.com",
        messagingSenderId: "71729910316",
        appId: "1:71729910316:web:d290cf7df7a6b507927789",
        measurementId: "G-VLCCSP6X8Z"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    firebase.analytics();
    </script>

</body>
</html>